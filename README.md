# Getting Started


## Setup Rust Env
https://docs.substrate.io/v3/getting-started/installation/

Make sure you have ~10GB of free space. or you will run out.

useful commands
```
# alwasy get the latest nightly substrate
rustup update
rustup update nightly
rustup target add wasm32-unknown-unknown --toolchain nightly

# Rust nightly toolchain
# Specify the specific nightly toolchain in the date below:
rustup install nightly-<yyyy-MM-dd>

# wasm toolchain
# Now, configure the nightly version to work with the Wasm compilation target:
rustup target add wasm32-unknown-unknown --toolchain nightly-<yyyy-MM-dd>

# Use the WASM_BUILD_TOOLCHAIN environment variable to specify the Rust nightly version a Substrate project should use for Wasm compilation:
WASM_BUILD_TOOLCHAIN=nightly-<yyyy-MM-dd> cargo build --release
```

## Actual Tutorial and sample contract

https://docs.substrate.io/tutorials/v3/ink-workshop/pt1/

### Notes
if on ubuntu don't instal binaryen via apt. it is out of date. Go here https://github.com/WebAssembly/binaryen/releases and put the binary on your path

## Running stuff
Need to start local substrate contract node
```
substrate-contracts-node --dev --tmp
# or for needing non local access
substrate-contracts-node --dev --tmp --ws-external
```

Need to start Contract UI
```
# clone https://github.com/paritytech/contracts-ui
# make sure have more than 2GB of RAM
go to folder and do `yarn && yarn start`
```




# Demo Notes
can make

https://docs.substrate.io/tutorials/v3/ink-workshop/pt2/
> ink! is just standard Rust in a well defined "contract format" with specialized #[ink(...)] attribute macros. These attribute macros tell ink! what the different parts of your Rust smart contract represent, and ultimately allow ink! to do all the magic needed to create Substrate compatible Wasm bytecode

framework comes with magic!

highlight rust constructs used.
- traits
- attributes
    - inner
        - meta cfg_attr (wtf?)
    - outter
        - different ink types
        - derive
        - more cfg (wtf?)
- closures
    - used in initialize_contract call
- options
    - used in events

still not clear on when borrowing is required or not


significance of message vs transaction

each contract can be thought of as a singleton and transactions are processed squentially
Functions are atomic.



# Appendix

## VS Code
If doing over remote SSH need to add to remote settings.json file

```
"rust-client.rustupPath": "/home/XXX/.cargo/bin/rustup"
```