#![cfg_attr(not(feature = "std"), no_std)]

use ink_lang as ink;

const DEFAULT_INCREMENTER_VALUE: i32 = 0;

#[ink::contract]
mod incrementer {
    use ink_storage::{lazy::Mapping, traits::SpreadAllocate};

    #[ink(storage)]
    #[derive(Default, SpreadAllocate)] // wtf is this
    pub struct Incrementer {
        // Storage Declaration
        value: i32,
        balance_map: Mapping<AccountId, i32>,
    }

    impl Incrementer {
        /// Constructor that initializes the `u32` value to the given `init_value`.
        #[ink(constructor)]
        pub fn new(init_value: i32) -> Self {
            ink_lang::codegen::initialize_contract(|contract: &mut Self| {
                contract.value = init_value;
                contract.balance_map = Default::default();
            })
        }

        /// Constructor that initializes the `u32` value to the `u32` default.
        ///
        /// Constructors can delegate to other constructors.
        #[ink(constructor)]
        pub fn default() -> Self {
            // not clear why we need to use init contract
            // example in docs say:
            // > This call is required in order to correctly initialize the `Mapping`s of our contract.
            // the pipes are for a closure, basically a lambda function
            ink_lang::codegen::initialize_contract(|contract: &mut Self| {
                // without super we get a `not found in scope error`
                contract.value = super::DEFAULT_INCREMENTER_VALUE;
                contract.balance_map = Default::default();
            })
            /*
            an example where we have an actual function
            ink_lang::codegen::initialize_contract(|contract| {
                Self::new_init(contract, initial_supply)
            })
            */
        }

        #[ink(message)]
        pub fn get(&self) -> i32 {
            // Contract Message
            self.value
        }

        #[ink(message)]
        pub fn inc(&mut self, by: i32) {
            self.value = self.value + by;
        }

        #[ink(message)]
        pub fn get_mine(&self) -> i32 {
            let caller = self.env().caller();
            self.my_value_or_zero(&caller)
            // ACTION: Get `my_value` using `my_value_or_zero` on `&self.env().caller()`
            // ACTION: Return `my_value`
        }

        #[ink(message)]
        pub fn inc_mine(&mut self, by: i32) {
            // ACTION: Get the `caller` of this function.
            let caller = self.env().caller();
            // ACTION: Get `my_value` that belongs to `caller` by using `my_value_or_zero`.
            let balance = self.my_value_or_zero(&caller);
            // ACTION: Insert the incremented `value` back into the mapping.
            // insert expect a reference to the value so we wrap the i32 value in a `&`
            self.balance_map.insert(&caller, &(balance + by));
        }

        fn my_value_or_zero(&self, of: &AccountId) -> i32 {
            // ACTION: `get` and return the value of `of` and `unwrap_or` return 0
            let balance = self.balance_map.get(of).unwrap_or(0);
            balance
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use ink_lang as ink;

        #[ink::test]
        fn default_works() {
            // Test Your Contract
            let incrementer_contract = Incrementer::default();
            assert_eq!(incrementer_contract.get(), 0)
        }

        #[ink::test]
        fn new_works() {
            // Test Your Contract
            let vals_to_test: [i32; 5] = [0, 5, 77777, -5, -99999];
            for v in vals_to_test {
                let incrementer_contract = Incrementer::new(v);
                assert_eq!(incrementer_contract.get(), v)
            }
        }

        #[ink::test]
        fn inc_works() {
            let mut contract = Incrementer::new(42);
            assert_eq!(contract.get(), 42);
            contract.inc(5);
            assert_eq!(contract.get(), 47);
            contract.inc(-50);
            assert_eq!(contract.get(), -3);
        }

        // Use `ink::test` to initialize accounts.
        #[ink::test]
        fn my_value_works() {
            let mut contract = Incrementer::new(11);
            assert_eq!(contract.get(), 11);
            assert_eq!(contract.get_mine(), 0);
            contract.inc_mine(5);
            assert_eq!(contract.get_mine(), 5);
            contract.inc_mine(10);
            assert_eq!(contract.get_mine(), 15);
        }
    }
}
